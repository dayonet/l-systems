import * as THREE from "./node_modules/three/src/Three.js";
import { LineSegments } from "./node_modules/three/src/Three.js";
// defining Stack data structure
class Stack {
    constructor() {
        this._store = [];
    }
    push(val) {
        this._store.push(val);
    }
    pop() {
        return this._store.pop();
    }
}
// SingleTree class
export class SingleTree {
    // constructor
    constructor(treeAlg, length) {
        this.treeAlgorithm = treeAlg;
        this.lineMaterial = new THREE.LineBasicMaterial({ color: "green" });
        this.length = length;
        this.points = [];
    }
    // finds point at a given distance from a given point at a given angle. Returns targer point that has been calculated
    findNextPoint(startingPoint, angle, length) {
        let newPointX = startingPoint.x + Math.cos((Math.PI / 180) * (angle)) * length; // x coordinate of targer point
        let newPointZ = startingPoint.z + Math.sin((Math.PI / 180) * (angle)) * length; // y coordinate of targer point
        return {
            x: newPointX,
            y: startingPoint.y,
            z: newPointZ
        };
    }
    // adds one line to points array. Receives starting point. Calculates next point using findNextPoint.
    drawLine(startingPoint, angle) {
        this.points.push(new THREE.Vector3(startingPoint.x, startingPoint.y, startingPoint.z));
        let p = this.findNextPoint(startingPoint, angle, this.length);
        this.points.push(new THREE.Vector3(p.x, p.y, p.z));
    }
    // expand algorithm string once. Receives current angorithm string and runs the production rules on it once. Returns string after expanding it once.
    applyProductionRulesOnce(current) {
        let result = ""; // result string
        for (let i = 0; i < current.length; i++) {
            let ch = current[i]; // current charecter in string
            let rule = this.treeAlgorithm.productionRules[ch];
            if (rule) {
                result += rule;
            }
            else {
                result += ch;
            }
        }
        return result;
    }
    // expand string a given number of times. Receives the number of iterations and returns the final string. Starts the expantion from the axion of the algorithm.
    applyProductionRules(numIterations) {
        let current = this.treeAlgorithm.axiom; // current string. Initial value is the axiom.
        for (let index = 0; index < numIterations; index++) {
            current = this.applyProductionRulesOnce(current);
        }
        return current;
    }
    // returns lineSegments object based on points calculated from the tree algorithm. Receives the number of iterstions and the starting point of the tree.
    drawL_system(numIterations, startingPoint) {
        let str = this.applyProductionRules(numIterations); // final algorithm string to draw
        console.log(this.treeAlgorithm.angle);
        console.log(str);
        let curAngle = 90; // current angle
        let anglePoint = {
            angle: curAngle,
            point: startingPoint
        };
        let stk = new Stack(); // stack of AngleAndPoint of algorithm in order to interpret '[' ']' characters in string.
        stk.push(anglePoint);
        // interpretation of the string
        for (let index = 0; index < str.length; index++) {
            const char = str[index];
            if (this.treeAlgorithm.draw.includes(char)) {
                this.drawLine(startingPoint, curAngle);
                startingPoint = this.findNextPoint(startingPoint, curAngle, this.length);
            }
            else if (char == "+") {
                curAngle += this.treeAlgorithm.angle;
            }
            else if (char === "−" || char === '-') {
                curAngle -= this.treeAlgorithm.angle;
            }
            else if (char == "[") {
                let ap = {
                    angle: curAngle,
                    point: startingPoint
                };
                stk.push(ap);
            }
            else if (char == "]") {
                let ap = stk.pop();
                curAngle = ap.angle;
                startingPoint = ap.point;
            }
            else {
                alert(`String result invalid. Character ${char} at index ${index} not recognized (draw is ${this.treeAlgorithm.draw})`);
            }
        }
        const geometry = new THREE.BufferGeometry().setFromPoints(this.points);
        let tree = new LineSegments(geometry, this.lineMaterial);
        return tree;
    }
}
//# sourceMappingURL=single-tree.js.map