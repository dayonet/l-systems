//// <reference path="node_modules/three/src/Three.d.ts" />
//// <reference path="node_modules/three/examples/jsm/math/MeshSurfaceSampler.d.ts" />

import * as THREE from "./node_modules/three/src/Three.js";
import { MeshSurfaceSampler } from "./node_modules/three/examples/jsm/math/MeshSurfaceSampler.js";
import { ClampToEdgeWrapping } from "./node_modules/three/src/Three.js";
//import { } from "./node_modules/three/src/";

type Point = { x: number, y: number };

interface Algorithm {
	description: string;
	name: string;
	axiom: string;
	productionRules: { [key: string]: string };
	angle: number;
	draw: string;
}

interface AlgorithmDictionary {
	[name: string]: Algorithm;
}

let algDic: AlgorithmDictionary;
function run(): void {
	$.getJSON(`./trees_algorithm_data.json`)
		.done((data: any) => {
			const $select = $("#l-systems");
			for (let key in data) {
				$("<option />", { value: key, text: data[key].description }).appendTo($select);
			}
			algDic = data;
		});

	$("#l-systems").on("change", () => {
		const key: any = $("#l-systems").val();
		const algorithm = algDic[key];
		if (algorithm) { // otherwise the chosen item doesn't point to an algorithm
			console.log("selected", key, "axiom", algorithm.axiom);
			let current: string = algorithm.axiom;
			for (let index = 0; index < 3; index++) {
				current = applyProductionRules(current, algorithm);
			}
			drawL_system(context, current, algorithm);
		}

	});

	const canvas = $("#gl-canvas")[0] as HTMLCanvasElement;
	// Initialize the GL context
	const context = canvas.getContext("2d");

	// Only continue if WebGL is available and working
	if (context === null) {
		alert("Unable to initialize WebGL. Your browser or machine may not support it.");
		return;
	}

	$("#clear").on("click", () => {
		clearCanvas(context);
	});

	const scene = new THREE.Scene();
	const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

	const renderer = new THREE.WebGLRenderer();
	renderer.setSize(window.innerWidth, window.innerHeight);
	document.body.appendChild(renderer.domElement);


	let n: number = 64;

	var geometry = new THREE.PlaneGeometry(4.5, 4.5, n, n);
	let initialHeight = [60, 100, -80, 34];
	let d: DiamondSquare = new DiamondSquare(n, n * n / 4, initialHeight);
	let data: number[] = d.getOneDimArrData();

	let m: number = 0;
	for (let i = 0, l = data.length; i < l; i++) {
		if (Math.abs(data[i]) > m) {
			m = Math.abs(data[i]);
		}

	}
	for (var i = 0, l = geometry.vertices.length; i < l; i++) {
		geometry.vertices[i].z = data[i] / m;
	}

	var material = new THREE.MeshBasicMaterial({ color: 0xdddddd, wireframe: true });
	var plane = new THREE.Mesh(geometry, material);

	scene.add(plane);

	camera.position.set(0, 1.6, 4.5);
	plane.rotation.x += Math.PI / 2;
	camera.rotation.x-= 0.12;
	renderer.render(scene, camera);


	let speed: number = 0.04;

	document.addEventListener("keydown", onDocumentKeyDown, false);
	function onDocumentKeyDown(event: any) {
		var keyCode = event.which;
		// w,a,s,d arrows (up down left & right)
		if (keyCode == 87) {
			camera.position.y += speed;
		}
		else if (keyCode == 83) {
			camera.position.y -= speed;
		}
		else if (keyCode == 65) {
			camera.position.x -= speed;
		}
		else if (keyCode == 68) {
			camera.position.x += speed;
		}
		// f for foward, g for backwards (depth)
		else if (keyCode == 70) {
			camera.position.z -= speed;
		}
		else if (keyCode == 71) {
			camera.position.z += speed;
		}
		// x for rotation right, z for rotation left
		else if (keyCode == 88) {
			camera.rotation.y -= speed;
		}
		else if (keyCode == 90) {
			camera.rotation.y += speed;
		}
		// l for rotation right,  for rotation left
		else if (keyCode == 77) {
			camera.rotation.x -= speed;
		}
		else if (keyCode == 75) {
			camera.rotation.x += speed;
		}
		// space for re-centering
		else if (keyCode == 32) {
			camera.position.set(0, 1.4, 4.5);
		}
		renderer.render(scene, camera);
	};
	document.addEventListener( 'mousemove', ( event ) => {

		if ( document.pointerLockElement === document.body ) {

			camera.rotation.y -= event.movementX / 500;
			camera.rotation.x -= event.movementY / 500;

		}

	} );
};

function clearCanvas(ctx: CanvasRenderingContext2D): void {
	// Set clear color to black, fully opaque
	ctx.fillStyle = "#fff";
	ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}

function findNextPoint(startingPoint: Point, angle: number, length: number): Point {
	let newPointX: number = startingPoint.x + Math.cos((Math.PI / 180) * (angle)) * length;
	let newPointY: number = startingPoint.y + Math.sin((Math.PI / 180) * (angle)) * length;
	return {
		x: newPointX,
		y: newPointY
	}
}

function drawLine(ctx: CanvasRenderingContext2D, startingPoint: Point, length: number, angle: number): void {
	ctx.strokeStyle = 'black';
	ctx.beginPath();
	ctx.moveTo(startingPoint.x, startingPoint.y);
	let p: Point = findNextPoint(startingPoint, angle, length);
	ctx.lineTo(p.x, p.y);
	ctx.stroke();
}


function applyProductionRules(current: string, alg: Algorithm): string {
	let result: string = "";
	for (let i: number = 0; i < current.length; i++) {
		let ch: string = current[i];
		let rule: string = alg.productionRules[ch];
		if (rule) {
			result += rule;
		}
		else {

			result += ch;
		}
	}
	return result;
}

function drawL_system(ctx: CanvasRenderingContext2D, str: String, a: Algorithm) {
	console.log(a.angle);
	clearCanvas(ctx);
	console.log(str);
	let curAngle = 0; // current angle
	let p: Point = {
		x: 600,
		y: 400
	}
	for (let index = 0; index < str.length; index++) {
		const char = str[index];
		if (a.draw.includes(char)) {
			drawLine(ctx, p, 10, curAngle);
			p = findNextPoint(p, curAngle, 10);

		}
		else if (char == "+") {
			curAngle += a.angle;
		}
		else if (char === "−" || char === '-') {
			curAngle -= a.angle;
		}
		else {
			alert(`String result invalid. Character ${char} at index ${index} not recognized (draw is ${a.draw})`);
		}
	}
}
$(run)