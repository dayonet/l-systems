//// <reference path="node_modules/three/src/Three.d.ts" />
//// <reference path="node_modules/three/examples/jsm/math/MeshSurfaceSampler.d.ts" />
import * as THREE from "./node_modules/three/src/Three.js";
import { Octree } from "./lib/octree.js";
import { Vector3 } from "./node_modules/three/src/Three.js";
import { DebugPanel } from "./debug-panel.js";
import { DebugMarker } from "./debug-marker.js";
const PLAYER_RADIUS = 0.3;
function run() {
    const debugPanel = new DebugPanel(".debug-panel-container");
    THREE.Vector3.prototype.toString = function () {
        return `Vector 3 X: ${this.x.toFixed(2)} Y:${this.y.toFixed(2)} Z: ${this.z.toFixed(2)}`;
    };
    const scene = new THREE.Scene();
    const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);
    const renderer = new THREE.WebGLRenderer();
    renderer.setSize(window.innerWidth, window.innerHeight);
    document.body.appendChild(renderer.domElement);
    const worldOctree = new Octree(null);
    //const playerCollider = new Capsule(new THREE.Vector3(0, 1.1, 4.5), new THREE.Vector3(0, 1.4, 4.5), 0.35);
    const sphereGeometry = new THREE.SphereGeometry(PLAYER_RADIUS, 16);
    const sphereMaterial = new THREE.MeshStandardMaterial({ color: 0x0000ff, opacity: 0.5, transparent: true, metalness: 0.5 });
    drawAnchorPoints(scene);
    // const sphereGeometry = new THREE.SphereGeometry(0.2, 32, 32);
    // const sphereMaterial = new THREE.MeshStandardMaterial({ color: 0x0000ff, transparent: true, opacity: 0.5 });
    // const sphereMaterial = new THREE.MeshStandardMaterial({ color: "red", roughness: 0.8, metalness: 0.5 });
    const player = {
        mesh: new THREE.Mesh(sphereGeometry, sphereMaterial),
        collider: new THREE.Sphere(new THREE.Vector3(0, 1.4, 4.5), PLAYER_RADIUS)
    };
    // const playerCollider = new THREE.Mesh(sphereGeometry, sphereMaterial);
    player.mesh.castShadow = true;
    player.mesh.receiveShadow = true;
    scene.add(player.mesh);
    // const ambientlight = new THREE.AmbientLight(0x6688cc, 5);
    // scene.add(ambientlight);
    const ambientlight = new THREE.AmbientLight(0x6688cc);
    scene.add(ambientlight);
    const fillLight1 = new THREE.DirectionalLight(0xff9999, 0.5);
    fillLight1.position.set(-1, 1, 2);
    scene.add(fillLight1);
    const fillLight2 = new THREE.DirectionalLight(0x8888ff, 0.2);
    fillLight2.position.set(0, -1, 0);
    scene.add(fillLight2);
    const directionalLight = new THREE.DirectionalLight(0xffffaa, 1.2);
    directionalLight.position.set(-5, 25, -1);
    directionalLight.castShadow = true;
    directionalLight.shadow.camera.near = 0.01;
    directionalLight.shadow.camera.far = 500;
    directionalLight.shadow.camera.right = 30;
    directionalLight.shadow.camera.left = -30;
    directionalLight.shadow.camera.top = 30;
    directionalLight.shadow.camera.bottom = -30;
    directionalLight.shadow.mapSize.width = 1024;
    directionalLight.shadow.mapSize.height = 1024;
    directionalLight.shadow.radius = 4;
    directionalLight.shadow.bias = -0.00006;
    scene.add(directionalLight);
    var cube = new THREE.Mesh(new THREE.BoxGeometry(1, 1, 1), new THREE.MeshStandardMaterial({ color: 0x9999ff, roughness: 0.8, metalness: 0.1, opacity: 0.5, transparent: true }));
    cube.position.x = cube.position.y = cube.position.z = 0;
    // add the object to the scene
    scene.add(cube);
    worldOctree.fromMesh(cube);
    //worldOctree.fromGraphNode( scene );
    // camera.position.copy(player.collider.center);
    let backWards = 1;
    camera.position.copy(player.collider.center);
    player.mesh.position.copy(player.collider.center);
    camera.position.z += backWards;
    // camera.position.z+=0.5;
    //camera.position.set(0, 1.4, 4.5);
    camera.rotation.x -= 0.12;
    renderer.render(scene, camera);
    let speed = 0.04;
    document.body.addEventListener('mousemove', (event) => {
        if (document.pointerLockElement === document.body) {
            camera.rotation.y -= event.movementX / 500;
            camera.rotation.x -= event.movementY / 500;
        }
    });
    document.addEventListener('mousemove', (event) => {
        if (document.pointerLockElement === document.body) {
            camera.rotation.y -= event.movementX / 500;
            camera.rotation.x -= event.movementY / 500;
        }
    });
    document.addEventListener("keydown", playerKeyDown, false);
    function playerKeyDown(event) {
        const keyCode = event.which;
        let translateX = 0, translateY = 0, translateZ = 0;
        // w,a,s,d arrows (up down left & right)
        if (keyCode == 87) {
            translateY = speed;
        }
        else if (keyCode == 83) {
            translateY = -speed;
        }
        else if (keyCode == 65) {
            translateX = -speed;
        }
        else if (keyCode == 68) {
            translateX = speed;
        }
        // f for foward, g for backwards (depth)
        else if (keyCode == 70) {
            translateZ = -speed;
        }
        else if (keyCode == 71) {
            translateZ = speed;
        }
        // x for rotation right, z for rotation left
        if (keyCode == 88) {
            camera.rotation.y -= speed;
        }
        else if (keyCode == 90) {
            camera.rotation.y += speed;
        }
        // // space for re-centering
        // else if (keyCode == 32) {
        // 	camera.position.set(0, 1.4, 4.5);
        // }
        let playerOnFloor = false;
        if (translateZ || translateY || translateX) {
            player.collider.translate(new THREE.Vector3(translateX, translateY, translateZ));
            camera.position.copy(player.collider.center);
            player.mesh.position.copy(player.collider.center);
            camera.position.z += backWards;
            // camera.position.y+=0.3
            // camera.position.z+=0.1;
            // camera.position.y+=0.3
            const result = worldOctree.sphereIntersect(player.collider);
            // const result = worldOctree.capsuleIntersect(playerCollider);
            playerOnFloor = false;
            if (result) {
                const message = `collision info:\ndepth = ${result.depth}\n\nmy position = ${player.mesh.position}`;
                debugPanel.log(message);
                // playerOnFloor = result.normal.y > 0;
                // if (!playerOnFloor) {
                player.collider.translate(result.normal.multiplyScalar(result.depth));
            }
            else {
                debugPanel.clear();
                //(plane.material as THREE.MeshBasicMaterial).color.setRGB(0xDD, 0xDD, 0xDD);
            }
        }
        renderer.render(scene, camera);
    }
    ;
    //plane.rotation.y += -0.5;
    //	if ( document.pointerLockElement === document.body ) {
    //		camera.rotation.y -= event.movementX / 500;
    //		camera.rotation.x -= event.movementY / 500;
    //	}
    //} );
    function animate() {
        camera.position.copy(player.collider.center);
        player.mesh.position.copy(player.collider.center);
        camera.position.z += backWards;
        // camera.position.copy(player.collider.center);
        // camera.position.z+=0.5;
        requestAnimationFrame(animate);
    }
    animate();
}
;
function clearCanvas(ctx) {
    // Set clear color to black, fully opaque
    ctx.fillStyle = "#fff";
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
}
function findNextPoint(startingPoint, angle, length) {
    let newPointX = startingPoint.x + Math.cos((Math.PI / 180) * (angle)) * length;
    let newPointY = startingPoint.y + Math.sin((Math.PI / 180) * (angle)) * length;
    return {
        x: newPointX,
        y: newPointY
    };
}
function drawLine(ctx, startingPoint, length, angle) {
    ctx.strokeStyle = 'black';
    ctx.beginPath();
    ctx.moveTo(startingPoint.x, startingPoint.y);
    let p = findNextPoint(startingPoint, angle, length);
    ctx.lineTo(p.x, p.y);
    ctx.stroke();
}
function applyProductionRules(current, alg) {
    let result = "";
    for (let i = 0; i < current.length; i++) {
        let ch = current[i];
        let rule = alg.productionRules[ch];
        if (rule) {
            result += rule;
        }
        else {
            result += ch;
        }
    }
    return result;
}
function drawAnchorPoints(scene) {
    const m = new DebugMarker(scene);
    m.position = new Vector3(1, 1, 1);
    m.color = 0xFF0000;
}
function drawL_system(ctx, str, a) {
    console.log(a.angle);
    clearCanvas(ctx);
    console.log(str);
    let curAngle = 0; // current angle
    let p = {
        x: 600,
        y: 400
    };
    for (let index = 0; index < str.length; index++) {
        const char = str[index];
        if (a.draw.includes(char)) {
            drawLine(ctx, p, 10, curAngle);
            p = findNextPoint(p, curAngle, 10);
        }
        else if (char == "+") {
            curAngle += a.angle;
        }
        else if (char === "−" || char === '-') {
            curAngle -= a.angle;
        }
        else {
            alert(`String result invalid. Character ${char} at index ${index} not recognized (draw is ${a.draw})`);
        }
    }
}
$(run);
//# sourceMappingURL=test-octree.js.map