import * as THREE from "./node_modules/three/src/Three.js";
import { Octree } from "./lib/octree.js";
import { BufferGeometry, Sphere, Vector3 } from "./node_modules/three/src/Three.js";
import { TrackballControls } from "./lib/tctl.js";
import { SingleTree } from "./single-tree.js";

// defining IPlayer interface. Represents a player in the world.
interface IPlayer {
	mesh: THREE.Mesh;
	collider: THREE.Sphere;
}

// standard way to control camera movment with three.js
function createCameraControls(camera: THREE.Camera, root: HTMLElement): TrackballControls {

	const controls = new TrackballControls(camera, root);

	controls.rotateSpeed = 1.0;
	controls.zoomSpeed = 1.2;
	controls.panSpeed = 0.8;

	controls.keys = ['ArrowLeft', 'ArrowUp', "ArrowRight"];
	return controls;

}

// defining Stack data structure
class Stack<T> {
	_store: T[] = [];
	push(val: T) {
		this._store.push(val);
	}
	pop(): T | undefined {
		return this._store.pop();
	}
}

// defining Point as a type. Holds x,y,z coordinates (as numbers)
type Point = { x: number, y: number, z: number };

// defining AngleAndPoint type. Holds an angle (number) value and a Point value.
type AngleAndPoint = { angle: number, point: Point }

// defining Algorithm interface. Holds all algorithm information.
interface Algorithm {
	description: string;
	name: string;
	axiom: string;
	productionRules: { [key: string]: string };
	angle: number;
	stack: Stack<AngleAndPoint>;
	draw: string;
}

// defining AlgorithmDictionary interface. Holds all algorithms in a key-value data structure.
interface AlgorithmDictionary {
	[name: string]: Algorithm;
}

THREE.Vector3.prototype.toString = function () {
	return `Vector 3 X: ${this.x.toFixed} Y:${this.y.toFixed} Z: ${this.z.toFixed}`;
}


const scene = new THREE.Scene(); // scene of world (will include the terrain and the trees)
const aspect = window.innerWidth / window.innerHeight; // area of the world

const camera = new THREE.PerspectiveCamera(60, aspect, 1, 1000); // world camera
camera.position.z = 10;
camera.rotation.y = 0;

const renderer = new THREE.WebGLRenderer({ antialias: true }); // renderer - responsible for rendering (drawing) the scene
renderer.setPixelRatio(window.devicePixelRatio);
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);


let algDic: AlgorithmDictionary;
function run(): void {
	// get json data and input in into a scroll-down option list
	$.getJSON(`../trees_algorithm_data.json`)
		.done((data: any) => {
			const $select = $("#l-systems");
			for (let key in data) {
				$("<option />", { value: key, text: data[key].description }).appendTo($select);
			}
			algDic = data;
		});
	// when an algorithm is chosen from the list it is drawn in the world according to the percent it should take up in the terrain
	$("#l-systems").on("change", () => {
		const key: any = $("#l-systems").val(); //hold the name of the algorithm
		let percentCoveredInTrees: number = +$("#percent-covered-in-trees").val(); // percent of mesh points that have trees on it
		let length = 0.08; // length of initial tree segments
		const algorithm = algDic[key]; // current algorithm to be drawn
		let treeBase: Point; // base point (starting point) of the tree
		let numIterations: number = 3;
		let meshPoints = (diamondMesh.geometry as BufferGeometry).getAttribute('position').array; // points of mesh (terrain)
		let covered = Math.round(meshPoints.length * (percentCoveredInTrees / 1500)); // num points on terrain that have trees on them
		let tree: SingleTree = new SingleTree(algorithm, length); // current tree to be drawn
		// loop for drawing all trees
		for (let index = 0; index < covered; index++) {
			console.log("selected", key, "axiom", algorithm.axiom);
			treeBase = randomizeTreePoints(diamondMesh);
			scene.add(tree.drawL_system(numIterations, treeBase));
		}
		renderer.render(scene, camera) // render scene with trees and terrain
	});

	// clears whole scene
	$("#clear").on("click", () => {
		clearCanvas(scene);
		renderer.render(scene, camera)
	});



	// clears whole scene
	function clearCanvas(scene: THREE.Scene): void {
		scene.clear();
	}
	const n: number = 128; // number of segments per side
	const worldSize: number = 20; // terrain worls size

	let initialHeight = [60, 100, -80, 34]; // initial hight for terrain corners
	let d: DiamondSquare = new DiamondSquare(n, n * n / 4, initialHeight);
	let data: number[] = d.getOneDimArrData(); // terrein height map from Diamond Square algorithm

	let m: number = Number.NEGATIVE_INFINITY;
	for (let i = 0, l = data.length; i < l; i++) {
		m = Math.max(m, data[i]);
	}

	let p1: Point;
	const halfBox = worldSize * 0.5; // holds half of world size
	const worldBox = new THREE.Box3(new Vector3(-halfBox, -halfBox, 0), new Vector3(halfBox, halfBox, 0)); // bounding box of future terrain
	const diamondMesh = createMesh(data, n, n, worldBox); // terrain as mesh
	scene.add(diamondMesh); // add terrain to scene

	const worldOctree = new Octree(null);

	// sphere to represent player
	const sphereGeometry = new THREE.SphereGeometry(0.1, 16);
	const sphereMaterial = new THREE.MeshStandardMaterial({ color: 0x0000ff, roughness: 0.8, metalness: 0.5, opacity: 0.5 });

	// player - camera does not have collision detection so player is used for that
	const player: IPlayer = {
		mesh: new THREE.Mesh(sphereGeometry, sphereMaterial),
		collider: new THREE.Sphere(new THREE.Vector3(1, -3.4, 4.5), 0.1)
	}

	player.mesh.castShadow = true;
	player.mesh.receiveShadow = true;
	scene.add(player.mesh);


	// world lighting
	const ambientLight = new THREE.AmbientLight('white', 0.5); // fills whole world with light
	const mainLight = new THREE.DirectionalLight("yellow", 2); // light from a specific direction
	mainLight.position.set(0, 3, 15);

	scene.add(ambientLight, mainLight);

	worldOctree.fromMesh(diamondMesh);
	camera.position.copy(player.collider.center);
	camera.position.setY(camera.position.y - 2);
	player.mesh.position.copy(player.collider.center);

	const cameraControls = createCameraControls(camera, renderer.domElement);
	// set a target point 4 meters ahead of the player
	cameraControls.target.set(player.collider.center.x, player.collider.center.y + 4, player.collider.center.z);
	// renderer.render(scene, camera);

	let speed: number = 0.1; // player movment speed

	// creates terrain mesh according to given heightmap fromDiamond Square algorithm
	function createMesh(data: Array<number>, width: number, height: number,
		worldRect: THREE.Box3): THREE.Mesh {
		// function has complex point calculations since mesh is made of triangles and the data we have refers to a sqare-based surface 
		const geometry = new THREE.BufferGeometry(); // mesh geometry
		const arr: Array<number> = []; // array of final values

		// world size definitions
		const worldSize = worldRect.getSize(new Vector3()),
			worldCenter = worldRect.getCenter(new Vector3());
		const tileWidth = worldSize.x / width,
			tileHeight = worldSize.y / height;

		const xMin = worldCenter.x - worldSize.x * 0.5; // min value of height of point
		const yMax = worldCenter.y + worldSize.y * 0.5; // max value of height of point

		let x: number, x1: number,
			y: number, y1: number;

		// z valuye of top left, top right, bottom left & bottom right corners accordingly
		let zTL: number, zTR: number,
			zBL: number, zBR: number;

		// normalize z values from heiht map
		function normalizeZ(z: number) {
			const invM = 1.1 / m;
			return worldCenter.z + z * invM;
		}
		// runs on all "rows & cols" of terrain
		for (let row = 0; row < height; ++row) {
			for (let col = 0; col < width; col++) {
				x = xMin + col * tileWidth;
				y = yMax - row * tileHeight;
				x1 = xMin + (col + 1) * tileWidth;
				y1 = yMax - (row + 1) * tileHeight;

				// defining corners of square
				zTL = normalizeZ(data[row * (width + 1) + col]);
				zTR = normalizeZ(data[row * (width + 1) + col + 1]);
				zBR = normalizeZ(data[(1 + row) * (width + 1) + col + 1]);
				zBL = normalizeZ(data[(1 + row) * (width + 1) + col]);
				// first triangle 
				/*
					   /|
					  / |
					 /  |
					/___|

				*/
				// בדיקת תקינות קלט
				if (isNaN(x) || isNaN(y)
					|| isNaN(x1) || isNaN(y1)
					|| isNaN(zTL) || isNaN(zTR)
					|| isNaN(zBR) || isNaN(zBL)) {
					debugger
				}
				arr.push(x, y1, zBL,
					x1, y1, zBR,
					x1, y, zTR);
				// second triangle 
				/*
					_____
					|   /
					|  /
					| /
					|/

				*/
				arr.push(x1, y, zTR,
					x, y, zTL,
					x, y1, zBL);
			}

		}
		const vertices = new Float32Array(arr);
		// itemSize = 3 because there are 3 values (components) per vertex
		geometry.setAttribute('position', new THREE.BufferAttribute(vertices, 3));
		const material = new THREE.MeshBasicMaterial({ color: "white", wireframe: true }); // terrain material
		const mesh = new THREE.Mesh(geometry, material); // terrain final mesh
		return mesh;
	}

	// calculates a random number in between two given numbers
	function rndBetween(min: number, max: number): number {
		return Math.floor(Math.random() * (max - min + 1)) + min;
	}

	// randomizes the points in which the trees are on the terrain
	function randomizeTreePoints(mesh: THREE.Mesh): Point {
		let startingPoint: Point; // starting point of tree
		let terrainAttributes = (mesh.geometry as BufferGeometry).getAttribute('position').array;
		let triangle_index: number = rndBetween(0, terrainAttributes.length / 3);
		startingPoint = {
			x: terrainAttributes[3 * triangle_index],
			y: terrainAttributes[3 * triangle_index + 1],
			z: terrainAttributes[3 * triangle_index + 2]
		}
		return startingPoint;
	}

	// פונקציה עזר לפונקציה שלא סיימתי לכתוב
	function randomizeAlgorithm() {
		let keys: string[] = Object.keys(algDic);
		const index = rndBetween(0, keys.length - 1);
		const key = keys[index];
		return algDic[key];
	}

	// פונקציה שלא סיימתי לכתוב, אחראית לבחירת עץ רנדומלי במקום שהמשתמש יבחר את סוג העץ
	function randomizeTrees() {
		let percentCoveredInTrees: number = +$("#percent-covered-in-trees").val();
		let length = 0.08;
		let algorithm: Algorithm;
		// const sceneTree = new THREE.Scene();
		let meshPoints = (diamondMesh.geometry as BufferGeometry).getAttribute('position').array;
		let covered = Math.round(meshPoints.length * (percentCoveredInTrees / 1500));
		let tree: SingleTree = new SingleTree(algorithm, length);
		for (let index = 0; index < covered; index++) {
			algorithm = randomizeAlgorithm();
			console.log("selected", algorithm.name, "axiom", algorithm.axiom);
			let current: string = algorithm.axiom;
			current = tree.applyProductionRules(3);
			p1 = randomizeTreePoints(diamondMesh);
			scene.add(tree.drawL_system(3, p1));
		}
		renderer.render(scene, camera)
	}

	// on movment events re-animate scene
	document.addEventListener("keydown", animate, false);
	document.addEventListener("keydown", playerKeyDown, false);
	document.addEventListener("mouseup", animate, false);

	// defining whet happens when a certin key is pressed
	function playerKeyDown(event: any) {
		const keyCode = event.which;
		let translateX = 0,
			translateY = 0,
			translateZ = 0;
		// i,j,k,l arrows (up down left & right)
		if (keyCode == 73) {
			translateY = speed;
		}
		else if (keyCode == 75) {
			translateY = -speed;
		}
		else if (keyCode == 74) {
			translateX = -speed;
		}
		else if (keyCode == 76) {
			translateX = speed;
		}
		// f for foward, g for backwards (depth)
		else if (keyCode == 70) {
			translateZ = -speed;
		}
		else if (keyCode == 71) {
			translateZ = speed;
		}
		else {
			translateX = 0;
		}
		const playerVelocity = new THREE.Vector3();
		if (translateZ || translateY || translateX) {
			(player.collider as any).translate(new THREE.Vector3(translateX, translateY, translateZ));
			const result = worldOctree.sphereIntersect(player.collider); // bool - was there a collision of not

			// if there was a collision - stop player movment
			if (result) {
				playerVelocity.addScaledVector(result.normal, - result.normal.dot(playerVelocity));

				player.collider.translate(result.normal.multiplyScalar(result.depth));

			}
		}
	};


	function animate(): void {
		if (!player.mesh.position.equals(player.collider.center)) {
			const delta = player.mesh.position.clone().sub(player.collider.center);
			player.mesh.position.sub(delta);
			camera.position.sub(delta);
			cameraControls.target.sub(delta);
		}
		cameraControls.update();
		renderer.render(scene, camera);
		requestAnimationFrame(animate);
	}
	animate();

};

$(run)