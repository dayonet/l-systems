import * as THREE from "./node_modules/three/src/Three.js";
import { LineSegments, Vector3 } from "./node_modules/three/src/Three.js";

// defining Stack data structure
class Stack<T>{
	_store: T[] = [];
	push(val: T) {
		this._store.push(val);
	}
	pop(): T | undefined {
		return this._store.pop();
	}
}

// defining Point as a type. Holds x,y,z coordinates (as numbers)
type Point = { x: number, y: number, z: number };

// defining AngleAndPoint type. Holds an angle (number) value and a Point value.
type AngleAndPoint = { angle: number, point: Point }

// defining Algorithm interface. Holds all algorithm information.
interface Algorithm {
	description: string;
	name: string;
	axiom: string;
	productionRules: { [key: string]: string };
	angle: number;
	stack: Stack<AngleAndPoint>;
	draw: string;
}

// SingleTree class
export class SingleTree {
	private treeAlgorithm: Algorithm; // algorithm of the tree
	private lineMaterial: THREE.LineBasicMaterial; // material of the tree (line material from three.js)
	private length: number; // line segment length
	private points: Vector3[];

	// constructor
	constructor(treeAlg: Algorithm, length: number) {
		this.treeAlgorithm = treeAlg;
		this.lineMaterial = new THREE.LineBasicMaterial({ color: "green" });
		this.length = length;
		this.points = [];
	}

	// finds point at a given distance from a given point at a given angle. Returns targer point that has been calculated
	private findNextPoint(startingPoint: Point, angle: number, length: number): Point {
		let newPointX: number = startingPoint.x + Math.cos((Math.PI / 180) * (angle)) * length; // x coordinate of targer point
		let newPointZ: number = startingPoint.z + Math.sin((Math.PI / 180) * (angle)) * length; // y coordinate of targer point
		return {
			x: newPointX,
			y: startingPoint.y,
			z: newPointZ
		}
	}

	// adds one line to points array. Receives starting point. Calculates next point using findNextPoint.
	private drawLine(startingPoint: Point, angle: number): void {
		this.points.push(new THREE.Vector3(startingPoint.x, startingPoint.y, startingPoint.z));
		let p: Point = this.findNextPoint(startingPoint, angle, this.length);
		this.points.push(new THREE.Vector3(p.x, p.y, p.z));
	}

	// expand algorithm string once. Receives current angorithm string and runs the production rules on it once. Returns string after expanding it once.
	private applyProductionRulesOnce(current: string): string {
		let result: string = ""; // result string
		for (let i: number = 0; i < current.length; i++) {
			let ch: string = current[i]; // current charecter in string
			let rule: string = this.treeAlgorithm.productionRules[ch];
			if (rule) {
				result += rule;
			}
			else {

				result += ch;
			}
		}
		return result;
	}

	// expand string a given number of times. Receives the number of iterations and returns the final string. Starts the expantion from the axion of the algorithm.
	public applyProductionRules(numIterations: number): string {
		let current: string = this.treeAlgorithm.axiom; // current string. Initial value is the axiom.
		for (let index = 0; index < numIterations; index++) {
			current = this.applyProductionRulesOnce(current);
		}
		return current;
	}

	// returns lineSegments object based on points calculated from the tree algorithm. Receives the number of iterstions and the starting point of the tree.
	public drawL_system(numIterations: number, startingPoint: Point): THREE.LineSegments {
		let str: string = this.applyProductionRules(numIterations) // final algorithm string to draw
		console.log(this.treeAlgorithm.angle);
		console.log(str);
		let curAngle = 90; // current angle
		let anglePoint: AngleAndPoint = { // angle amd point of algorithm
			angle: curAngle,
			point: startingPoint
		}
		let stk: Stack<AngleAndPoint> = new Stack(); // stack of AngleAndPoint of algorithm in order to interpret '[' ']' characters in string.
		stk.push(anglePoint);

		// interpretation of the string
		for (let index = 0; index < str.length; index++) {
			const char = str[index];
			if (this.treeAlgorithm.draw.includes(char)) {
				this.drawLine(startingPoint, curAngle);
				startingPoint = this.findNextPoint(startingPoint, curAngle, this.length);
			}
			else if (char == "+") {
				curAngle += this.treeAlgorithm.angle;
			}
			else if (char === "−" || char === '-') {
				curAngle -= this.treeAlgorithm.angle;
			}
			else if (char == "[") {
				let ap: AngleAndPoint = {
					angle: curAngle,
					point: startingPoint
				}
				stk.push(ap);
			}
			else if (char == "]") {
				let ap: AngleAndPoint = stk.pop();
				curAngle = ap.angle;
				startingPoint = ap.point;
			}
			else {
				alert(`String result invalid. Character ${char} at index ${index} not recognized (draw is ${this.treeAlgorithm.draw})`);
			}
		}
		const geometry = new THREE.BufferGeometry().setFromPoints(this.points);
		let tree: LineSegments = new LineSegments(geometry, this.lineMaterial);
		return tree;
	}
}