class DiamondSquare {
    // constructor
    constructor(pointsPerSide, roughness, initialHeight) {
        this.pointsPerSide = pointsPerSide;
        this.roughness = roughness;
        this.initialHeight = initialHeight;
    }
    // random number in between two given numbers
    rndBetween(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }
    // get data as two dimensional array
    getData() {
        return this.diamondSquareAlgorithm();
    }
    // get data as one dimensional array. From 2d array to 1D array.
    getOneDimArrData() {
        let flattened = [].concat.apply([], this.getData()); // 1D array of the height map
        return flattened;
    }
    //the algorithm itself, creates based on the parameters a hight map. Returns a 2D array of the height map.
    diamondSquareAlgorithm() {
        let dataSize = this.pointsPerSide + 1; // number of points per side - equal to the segments per side + 1
        let data = new Array(dataSize).fill(0).map(() => new Array(dataSize).fill(0)); // 2D array of height map  
        //set corner values
        data[0][0] = this.initialHeight[0];
        data[0][dataSize - 1] = this.initialHeight[1];
        data[dataSize - 1][0] = this.initialHeight[2];
        data[dataSize - 1][dataSize - 1] = this.initialHeight[3];
        let h = this.roughness; // offset range
        let r; // random number
        let cornersAvg = 0; // average of corners
        let midpointsAvg = 0; // average of midpoints of edges
        for (let sideLength = dataSize - 1; sideLength >= 2; Math.floor(sideLength /= 2), h /= 2) {
            let sideFraction = sideLength / 2;
            for (let x = 0; x < dataSize - 1; x += sideLength) {
                for (let y = 0; y < dataSize - 1; y += sideLength) {
                    cornersAvg = data[x][y] + data[x + sideLength][y] + data[x][y + sideLength] + data[x + sideLength][y + sideLength]; // sum all 4 corners
                    cornersAvg /= 4; // final average
                    data[x + sideFraction][y + sideFraction] = cornersAvg + this.rndBetween(-h, h); // center cell equal to avg + offset
                }
            }
            for (let x = 0; x < dataSize - 1; x += sideFraction) {
                for (let y = (x + sideFraction) % sideLength; y < dataSize - 1; y += sideLength) {
                    midpointsAvg = data[(x - sideFraction + dataSize) % dataSize][y] + data[(x + sideFraction) % dataSize][y] + data[x][(y - sideFraction + dataSize) % dataSize]; // sum of all 4 midpoints
                    midpointsAvg /= 4; // final average
                    data[x][y] = midpointsAvg + this.rndBetween(-h, h);
                    if (x == 0) {
                        data[dataSize - 1][y] = midpointsAvg;
                    }
                    if (y == 0) {
                        data[x][dataSize - 1] = midpointsAvg;
                    }
                }
            }
        }
        return data;
    }
}
//# sourceMappingURL=diamondSquare.js.map