import {
	Box3,
	Ray,
	Mesh,
	Line3,
	Object3D,
	Plane,
	Sphere,
	Triangle,
	Vector3,
	BufferGeometry,
	BoxGeometry
} from "../node_modules/three/src/Three.js";
import { Capsule } from './capsule.js';

export interface IntersectionResult {
	normal: Vector3;
	depth: number;
	point: Vector3;
}

export interface RayIntersectionResult {
	distance: number;
	triangle: Triangle;
	position: Vector3;
}

export class Octree {

	/**
	 * _v1 and the other _ vars should not be private, but this seems like a safe port from the original
	 * vars defined in the self-executing scope
	 */
	private _v1 = new Vector3();
	private _v2 = new Vector3();
	private _plane = new Plane();
	private _line1 = new Line3();
	private _line2 = new Line3();
	private _sphere = new Sphere();
	private _capsule = new Capsule();

	/**
	 * Real private vars
	 */
	private subTrees: Array<Octree>;
	private triangles: Array<Triangle>;
	private bounds: Box3;

	constructor(private box: Box3) {

		this.triangles = [];
		this.subTrees = [];

	}
	getBounds(){
		return this.bounds;
	}
	addTriangle(triangle: Triangle) {

		if (!this.bounds) {
			this.bounds = new Box3();
		}

		this.bounds.min.x = Math.min(this.bounds.min.x, triangle.a.x, triangle.b.x, triangle.c.x);
		this.bounds.min.y = Math.min(this.bounds.min.y, triangle.a.y, triangle.b.y, triangle.c.y);
		this.bounds.min.z = Math.min(this.bounds.min.z, triangle.a.z, triangle.b.z, triangle.c.z);
		this.bounds.max.x = Math.max(this.bounds.max.x, triangle.a.x, triangle.b.x, triangle.c.x);
		this.bounds.max.y = Math.max(this.bounds.max.y, triangle.a.y, triangle.b.y, triangle.c.y);
		this.bounds.max.z = Math.max(this.bounds.max.z, triangle.a.z, triangle.b.z, triangle.c.z);

		this.triangles.push(triangle);

		return this;

	}

	calcBox() {

		this.box = this.bounds.clone();

		// offset small ammount to account for regular grid
		this.box.min.x -= 0.01;
		this.box.min.y -= 0.01;
		this.box.min.z -= 0.01;

		return this;

	}

	split(level: number) {

		if (!this.box) return;

		const subTrees = [],
			halfsize = this._v2.copy(this.box.max).sub(this.box.min).multiplyScalar(0.5);
		let box: Box3, v: Vector3,
			triangle: Triangle;

		for (let x = 0; x < 2; x++) {

			for (let y = 0; y < 2; y++) {

				for (let z = 0; z < 2; z++) {

					box = new Box3();
					v = this._v1.set(x, y, z);

					box.min.copy(this.box.min).add(v.multiply(halfsize));
					box.max.copy(box.min).add(halfsize);

					subTrees.push(new Octree(box));

				}

			}

		}

		while (triangle = this.triangles.pop()) {

			for (var i = 0; i < subTrees.length; i++) {

				if (subTrees[i].box.intersectsTriangle(triangle)) {

					subTrees[i].triangles.push(triangle);

				}

			}

		}

		for (let i = 0; i < subTrees.length; i++) {

			const len = subTrees[i].triangles.length;

			if (len > 8 && level < 16) {

				subTrees[i].split(level + 1);

			}

			if (len != 0) {

				this.subTrees.push(subTrees[i]);

			}

		}

		return this;

	}

	build() {

		this.calcBox();
		this.split(0);

		return this;

	}

	getRayTriangles(ray: Ray, triangles: Array<Triangle>) {

		for (let i = 0; i < this.subTrees.length; i++) {

			const subTree = this.subTrees[i];
			if (!ray.intersectsBox(subTree.box)) continue;

			if (subTree.triangles.length > 0) {

				for (var j = 0; j < subTree.triangles.length; j++) {

					if (triangles.indexOf(subTree.triangles[j]) === - 1) triangles.push(subTree.triangles[j]);

				}

			} else {

				subTree.getRayTriangles(ray, triangles);

			}

		}

		return triangles;

	}

	triangleCapsuleIntersect(capsule: Capsule, triangle: Triangle): IntersectionResult {

		let point1, point2, line1, line2;

		triangle.getPlane(this._plane);

		const d1 = this._plane.distanceToPoint(capsule.start) - capsule.radius;
		const d2 = this._plane.distanceToPoint(capsule.end) - capsule.radius;

		if ((d1 > 0 && d2 > 0) || (d1 < - capsule.radius && d2 < - capsule.radius)) {

			return null;

		}

		const delta = Math.abs(d1 / (Math.abs(d1) + Math.abs(d2)));
		const intersectPoint = this._v1.copy(capsule.start).lerp(capsule.end, delta);

		if (triangle.containsPoint(intersectPoint)) {

			return { normal: this._plane.normal.clone(), point: intersectPoint.clone(), depth: Math.abs(Math.min(d1, d2)) };

		}

		const r2 = capsule.radius * capsule.radius;

		line1 = this._line1.set(capsule.start, capsule.end);

		const lines = [
			[triangle.a, triangle.b],
			[triangle.b, triangle.c],
			[triangle.c, triangle.a]
		];

		for (let i = 0; i < lines.length; i++) {

			line2 = this._line2.set(lines[i][0], lines[i][1]);

			[point1, point2] = capsule.lineLineMinimumPoints(line1, line2);

			if (point1.distanceToSquared(point2) < r2) {

				return { normal: point1.clone().sub(point2).normalize(), point: point2.clone(), depth: capsule.radius - point1.distanceTo(point2) };

			}

		}

		return null;

	}

	triangleSphereIntersect(sphere: Sphere, triangle: Triangle): IntersectionResult {

		triangle.getPlane(this._plane);

		if (!sphere.intersectsPlane(this._plane)) {
			return null;
		}

		var depth = Math.abs(this._plane.distanceToSphere(sphere));
		var r2 = sphere.radius * sphere.radius - depth * depth;

		var plainPoint = this._plane.projectPoint(sphere.center, this._v1);

		if (triangle.containsPoint(sphere.center)) {
			return { normal: this._plane.normal.clone(), point: plainPoint.clone(), depth: Math.abs(this._plane.distanceToSphere(sphere)) };

		}

		const lines = [
			[triangle.a, triangle.b],
			[triangle.b, triangle.c],
			[triangle.c, triangle.a]
		];

		for (let i = 0; i < lines.length; i++) {

			this._line1.set(lines[i][0], lines[i][1]);
			this._line1.closestPointToPoint(plainPoint, true, this._v2);

			const d = this._v2.distanceToSquared(sphere.center);

			if (d < r2) {

				return { normal: sphere.center.clone().sub(this._v2).normalize(), point: this._v2.clone(), depth: sphere.radius - Math.sqrt(d) };

			}

		}

		return null;

	}

	getSphereTriangles(sphere: Sphere, triangles: Array<Triangle>) {

		for (var i = 0; i < this.subTrees.length; i++) {

			var subTree = this.subTrees[i];

			if (!sphere.intersectsBox(subTree.box)) continue;

			if (subTree.triangles.length > 0) {

				for (var j = 0; j < subTree.triangles.length; j++) {

					if (triangles.indexOf(subTree.triangles[j]) === - 1) triangles.push(subTree.triangles[j]);

				}

			} else {

				subTree.getSphereTriangles(sphere, triangles);

			}

		}

	}

	getCapsuleTriangles(capsule: Capsule, triangles: Array<Triangle>) {

		for (let i = 0; i < this.subTrees.length; i++) {

			var subTree = this.subTrees[i];

			if (!capsule.intersectsBox(subTree.box)) continue;

			if (subTree.triangles.length > 0) {

				for (var j = 0; j < subTree.triangles.length; j++) {

					if (triangles.indexOf(subTree.triangles[j]) === - 1) triangles.push(subTree.triangles[j]);

				}

			} else {

				subTree.getCapsuleTriangles(capsule, triangles);

			}

		}

	}

	sphereIntersect(sphere: Sphere): IntersectionResult {

		this._sphere.copy(sphere);

		const triangles: Array<Triangle> = [];
		let result: IntersectionResult;
		let hit = false;

		this.getSphereTriangles(sphere, triangles);

		for (var i = 0; i < triangles.length; i++) {

			if (result = this.triangleSphereIntersect(this._sphere, triangles[i])) {

				hit = true;

				this._sphere.center.add(result.normal.multiplyScalar(result.depth));

			}

		}

		if (hit) {
			const collisionVector = this._sphere.center.clone().sub(sphere.center);
			const depth = collisionVector.length();

			return { normal: collisionVector.normalize(), depth: depth, point: null };

		}

		return null;

	}

	capsuleIntersect(capsule: Capsule): IntersectionResult {

		this._capsule.copy(capsule);

		const triangles: Array<Triangle> = [];
		let result: IntersectionResult;
		let hit = false;

		this.getCapsuleTriangles(this._capsule, triangles);

		for (let i = 0; i < triangles.length; i++) {

			if (result = this.triangleCapsuleIntersect(this._capsule, triangles[i])) {

				hit = true;

				this._capsule.translate(result.normal.multiplyScalar(result.depth));

			}

		}

		if (hit) {

			var collisionVector = this._capsule.getCenter(new Vector3()).sub(capsule.getCenter(this._v1));
			var depth = collisionVector.length();

			return { normal: collisionVector.normalize(), depth: depth, point: null };

		}

		return null;

	}

	rayIntersect(ray: Ray): RayIntersectionResult {

		if (ray.direction.length() === 0) {
			return;
		}

		const triangles: Array<Triangle> = [];
		let triangle: Triangle;
		let position: Vector3;
		let distance = 1e100;

		this.getRayTriangles(ray, triangles);

		for (let i = 0; i < triangles.length; i++) {

			const intersection = ray.intersectTriangle(triangles[i].a, triangles[i].b, triangles[i].c, true, this._v1);

			if (intersection) {

				const newdistance = intersection.sub(ray.origin).length();

				if (distance > newdistance) {

					position = intersection.clone().add(ray.origin);
					distance = newdistance;
					triangle = triangles[i];
				}

			}

		}

		return distance < 1e100 ? { distance, triangle, position } : null;

	}

	fromGraphNode(group: Object3D): this {

		group.traverse((obj) => {

			if ((obj && obj.type) === 'Mesh') {
				this.addMesh(obj as Mesh);
			}
			else {
				console.warn(`OCTree::fromGraphNode: don't know how to handle ${obj && obj.type}`);
			}
		});

		this.build();

		return this;

	}

	fromMesh(mesh: Mesh): this {
		this.addMesh(mesh);
		this.build();
		return this;
	}

	private addMesh(mesh: Mesh): void {
		mesh.updateMatrix();
		mesh.updateWorldMatrix(false, false);

		let geometry: BufferGeometry;
		let isTemp = false;

		if ((mesh.geometry as BoxGeometry).isGeometry) {
			isTemp = true;
			geometry = new BufferGeometry().fromGeometry(mesh.geometry as BoxGeometry);
		} 
		else {
			geometry = mesh.geometry as BufferGeometry;

		}

		const positions = geometry.attributes.position.array;
		const transform = mesh.matrixWorld;

		for (let i = 0; i < positions.length; i += 9) {

			const v1 = new Vector3(positions[i], positions[i + 1], positions[i + 2]);
			const v2 = new Vector3(positions[i + 3], positions[i + 4], positions[i + 5]);
			const v3 = new Vector3(positions[i + 6], positions[i + 7], positions[i + 8]);

			v1.applyMatrix4(transform);
			v2.applyMatrix4(transform);
			v3.applyMatrix4(transform);

			this.addTriangle(new Triangle(v1, v2, v3));

		}

		if (isTemp) {
			geometry.dispose();

		}
	}
}
