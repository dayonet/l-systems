import * as THREE from "./node_modules/three/src/Three.js";
export class DebugMarker {
    constructor(scene) {
        const sphereGeometry = new THREE.SphereGeometry(0.1, 16);
        const sphereMaterial = new THREE.MeshStandardMaterial({ color: 0xFFFFFF, roughness: 0.8, metalness: 0.5, opacity: 0.5 });
        this._mesh = new THREE.Mesh(sphereGeometry, sphereMaterial);
        scene.add(this._mesh);
    }
    get position() {
        const ret = new THREE.Vector3;
        ret.copy(this._mesh.position);
        return ret;
    }
    set position(pos) {
        this._mesh.position.copy(pos);
    }
    set color(color) {
        this._mesh.material.color.set(color);
    }
}
//# sourceMappingURL=debug-marker.js.map