import { Scene } from "three";
import * as THREE from "./node_modules/three/src/Three.js";

export class DebugMarker {

	private _position: THREE.Vector3;
	private _color: Number;
	private _mesh: THREE.Mesh;
	constructor(scene: THREE.Scene) {
		const sphereGeometry = new THREE.SphereGeometry(0.1, 16);
		const sphereMaterial = new THREE.MeshStandardMaterial({ color: 0xFFFFFF, roughness: 0.8, metalness: 0.5, opacity: 0.5 });
		this._mesh = new THREE.Mesh(sphereGeometry, sphereMaterial);
		scene.add(this._mesh);
	}

	public get position(): THREE.Vector3 {
		const ret = new THREE.Vector3;
		ret.copy(this._mesh.position);
		return ret;
	}

	public set position(pos: THREE.Vector3) {
		this._mesh.position.copy(pos);
	}

	public set color(color: number) {
		(this._mesh.material as THREE.MeshBasicMaterial).color.set(color);
	}

}