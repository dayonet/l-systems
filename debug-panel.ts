
const $: JQueryStatic = window["jQuery"];

export class DebugPanel {
    private ui: JQuery;
    private $log: JQuery;
    constructor(pageLocation: string) {
        this.ui = $(pageLocation).css({
            "background-color": "#eeeeff",
            "border": "1px solid blue",
            "padding": "5px"
        });
        this.$log = this.ui.find(".log");
        this.ui.find("#btn-clear-debug").click(() => {
            this.clear();
        });
    }

    log(message: string): void {
        this.$log.html(message);
    }

    clear(): void {
        this.log("");
    }
    
}