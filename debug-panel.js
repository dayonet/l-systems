const $ = window["jQuery"];
export class DebugPanel {
    constructor(pageLocation) {
        this.ui = $(pageLocation).css({
            "background-color": "#eeeeff",
            "border": "1px solid blue",
            "padding": "5px"
        });
        this.$log = this.ui.find(".log");
        this.ui.find("#btn-clear-debug").click(() => {
            this.clear();
        });
    }
    log(message) {
        this.$log.html(message);
    }
    clear() {
        this.log("");
    }
}
//# sourceMappingURL=debug-panel.js.map